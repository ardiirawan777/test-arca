<div>
    <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"> @lang('main.form.detail')</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
    </div>
    <div class="modal-body">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <th>Total</th>
                <td class="text-end" colspan="2">Rp.{{ rupiah_format($data->ammount) }}</td>
            </tr>
            <tr>
                <th colspan="3">List Pembayaran</th>
            </tr>
            <tr>
                <th>Nama</th>
                <th>Persen</th>
                <th>Jumlah</th>
            </tr>
            @foreach($data->employees as $employees)
                <tr>
                    <td>{{ $employees->name }}</td>
                    <td class="text-end">{{ $employees->percen }}%</td>
                    <td class="text-end">Rp.{{ rupiah_format($employees->ammount) }}</td>
                </tr>
            @endforeach

        </table>
    </div>
</div>
