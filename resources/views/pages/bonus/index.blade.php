@extends('layouts.admin.app')

@section("title","Bonus")

@section('page_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/js/sweetalert2/dist/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/lightbox2/css/lightbox.css') }}">

@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            <h5> Bonus </h5>
        </div>
        <div class="d-flex justify-content-between">
            <button class="btn btn-primary ms-4 show-form" data-url="{{ route('bonus.create') }}" data-size="lg">
                <i class="fa fa-plus"></i> Tambah
            </button>
        </div>
        <div class="card-body">

            <table id="myTable" class="table">
                <thead>
                <tr>
                    <th style="width: 100px;">No</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Total (Rp)</th>
                    <th class="text-center" style="width: 200px;">Aksi</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <x-modal  />
@endsection

@section('page_scripts')

    <script src="{{asset('assets/admin/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/sweetalert2/dist/sweetalert2.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/lightbox2/js/lightbox.js')}}"></script>
    <script src="{{ asset('assets/admin/plugins/AutoNumeric/AutoNumeric.js') }}"></script>
@endsection

@push('js')
    <script>

        $(document).ready(function() {

            var table = $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route("bonus.index") }}' ,
                columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                    {

                        data: 'created_at'
                    },
                    {
                        class: 'text-end',
                        data: 'ammount'
                    },
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false
                    }
                ]
            });
        });

        $('body').on("click", ".show-form", showForm);
        $('#modals').on("submit", ".forms", saveForm);
        $('body').on("click", ".btn_delete", deleteForm);
    </script>
@endpush
