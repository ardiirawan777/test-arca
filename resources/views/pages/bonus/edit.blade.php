<form   data-url="{{ route('bonus.update',['bonu' => $data]) }}" method="POST" class="needs-validation forms" novalidate>
    <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"> @lang('main.form.edit')</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
    </div>
    <div class="modal-body">
        <div class="errors"></div>

        @csrf
        @method('PUT')

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Pembayaran <x-requiredmark /></label>
            <div class="col-sm-10">
                <input  type="text" class="form-control pembayaran_show" value="{{ $data->ammount }}" required>
                <input name="ammount" type="hidden" class="form-control pembayaran" value="{{ $data->ammount }}" required>
            </div>
        </div>

        <button type="button" class="btn btn-primary add-employee">
            <i class="fa fa-plus me-2"></i>
        </button>

        <div class="row">
            <div id="input-employees">
                @foreach($data->employees as $i => $employee)
                    <div class="form-group row mt-0">

                        <div class="col-sm-6">
                            <label class="col-sm-12 col-form-label">Nama <x-requiredmark /></label>
                            <input name="name[]" type="text" value="{{$employee->name}}" class="form-control name-employee" data-order="{{ $i+1 }}" required>
                        </div>

                        <div class="col-sm-2">
                            <label class="col-sm-12 col-form-label">Persen <x-requiredmark /></label>
                            <input name="percen[]" type="number" value="{{$employee->percen}}" class="form-control percen-employee" data-order="{{ $i+1 }}" required>
                        </div>
                        <label class="col-sm-1 col-form-label mt-5">%</label>
                        <div class="col-sm-2 mt-5">
                            <button type="button" class="btn btn-danger delete-employee"> <i class="fa fa-remove"></i> </button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <h3>Total</h3>
        <hr>
        <div class="row">
            <div id="list-bonuses">
                @foreach($data->employees as $i => $employee)
                    <div class="row">
                        <div class="col-sm-1">
                            {{ $i+1 }}.
                        </div>
                        <div class="col-sm-3">
                            <div class="name-employee-{{$i+1}}">{{ $employee->name }}</div>
                        </div>
                        <div class="col-sm-7">
                            <div class="ammount-employee-{{$i+1}}">Rp. {{ rupiah_format($employee->ammount) }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


    </div>
    <div class="modal-footer">

        <div class="loading" style="display: none;">
            <div class="spinner-border text-primary" role="status" style="width: 30px; height: 30px; margin-right:20px">
            </div>
        </div>

        <div class="button w-100">
            <div class="d-flex justify-content-between ">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal"> <i class="fa fa-reply-all"></i> @lang('main.button.cancel')</button>
                <button type="submit" class="btn btn-success "> <i class="fa fa-paper-plane"></i> @lang('main.button.edit')</button>
            </div>
        </div>
    </div>
</form>

<script>
    var order = "{{ count($data->employees)+1 }}";
    autoNumericGlobal("pembayaran_show", "pembayaran")

    function validationAmmount(){
        var pembayaran = $('.pembayaran').val();

        if(pembayaran == 0){

            return false;
        }
        return true;
    }

    $('.add-employee').click(function (){
        var validate = validationAmmount();
        if(!validate){
            alert('Silahkan Input Nominal Pembayaran');
            return false;
        }

        // add dynamic input employee
        var input = `
                <div class="form-group row mt-0">

                    <div class="col-sm-6">
                        <label class="col-sm-12 col-form-label">Nama <x-requiredmark /></label>
                        <input name="name[]" type="text" class="form-control name-employee" data-order="`+order+`" required>
                    </div>

                    <div class="col-sm-2">
                        <label class="col-sm-12 col-form-label">Persen <x-requiredmark /></label>
                        <input name="percen[]" type="number"  class="form-control percen-employee" data-order="`+order+`" required>
                    </div>
                    <label class="col-sm-1 col-form-label mt-5">%</label>
                    <div class="col-sm-2 mt-5">
                        <button type="button" class="btn btn-danger delete-employee"> <i class="fa fa-remove"></i> </button>
                    </div>
                </div>`;


        $('#input-employees').append(input);



        //     add ammount list
        var ammounts = `
            <div class="row">
                    <div class="col-sm-1">
                        `+order+`.
                    </div>
                    <div class="col-sm-3">
                       <div class="name-employee-`+order+`"></div>
                    </div>
                    <div class="col-sm-7">
                       <div class="ammount-employee-`+order+`"></div>
                    </div>
            </div>`;

        $('#list-bonuses').append(ammounts);

        order++;


    });


    $(document).on('click', '.delete-employee', function(){
        $(this).parent().parent().remove()
    })

    $(document).on('keyup', '.name-employee', function(){
        var order = $(this).attr('data-order');

        var value = $(this).val();
        $('.name-employee-'+order).text(value);
    });

    $(document).on('keyup', '.percen-employee', function(){
        var order = $(this).attr('data-order');
        var totalAmmount = $('.pembayaran').val();

        var value = $(this).val();
        var percen = value / 100;
        total = percen * totalAmmount;
        $('.ammount-employee-'+order).text('Rp. ' + new Intl.NumberFormat('id-ID').format(total));
    });
</script>
