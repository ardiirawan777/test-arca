@extends('layouts.admin.app')

@section("title","Profile")


@section('content')
    <div class="card">
        <form method="POST" action="{{  url('profile/update-profile') }}">
            @csrf
            <div class="card-header">
                <h5> Profile </h5>
            </div>
            <div class="card-body">
                @if(Session::has('success-profile'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert"><strong>Berhasil ! </strong> Data Profile Berhasil diubah
                        <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close" data-bs-original-title="" title=""></button>
                    </div>
                @endif
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" value="{{ auth()->user()->name  }}" required>
                        @error("name")
                        tes
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control @error("email") is-invalid @enderror" name="email" value="{{ old("email") ?? auth()->user()->email  }}" required>
                        <input type="hidden" class="form-control " name="email_old" value="{{ auth()->user()->email  }}" required>
                        @error("email")
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger"><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer p-3">
                <button class="btn btn-primary" type="submit">
                    <i class="fa-solid fa-paper-plane"></i>  Ubah
                </button>
            </div>
        </form>
    </div>

    <div class="card">
        <form method="POST" action="{{  url('profile/update-password') }}">
            @csrf
            <div class="card-header">
                <h5> Ubah Password </h5>
            </div>
            <div class="card-body">
                @if(Session::has('success-password'))
                    <div class="alert alert-primary alert-dismissible fade show" role="alert"><strong>Berhasil ! </strong> Password Berhasil diubah
                        <button class="btn-close" type="button" data-bs-dismiss="alert" aria-label="Close" data-bs-original-title="" title=""></button>
                    </div>
                @endif
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Password </label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control @error("password") is-invalid @enderror" name="password"  required>
                        @error("password")
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger"><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Ulangi Password </label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control @error("repeat_password") is-invalid @enderror" name="repeat_password"  required>
                        @error("repeat_password")
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger"><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer p-3">
                <button class="btn btn-primary">
                    <i class="fa-solid fa-paper-plane"></i> Ubah
                </button>
            </div>
        </form>
    </div>
@endsection
