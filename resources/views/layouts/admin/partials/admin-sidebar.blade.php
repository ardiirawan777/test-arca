
<li class="dropdown">
    <a class="nav-link menu-title link-nav {{ Route::is('dashboard') ? 'active' : '' }}" href="{{ route('dashboard') }}">
        <i class="fa-solid fa-house me-2"></i>
        <span>Dashboard</span>
    </a>
</li>


<li class="dropdown">
    <a class="nav-link menu-title link-nav {{ Route::is('bonus.*') ? 'active' : '' }}" href="{{ route('bonus.index') }}">
        <i class="fa-solid fa-money-bill me-2"></i>
        <span>Bonus</span>
    </a>
</li>


