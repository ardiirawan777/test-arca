<script src="{{asset('assets/admin/js/jquery-3.5.1.min.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('assets/admin/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('assets/admin/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('assets/admin/js/sidebar-menu.js')}}"></script>
<script src="{{asset('assets/admin/js/config.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('assets/admin/js/bootstrap/popper.min.js')}}"></script>
<script src="{{asset('assets/admin/js/bootstrap/bootstrap.min.js')}}"></script>
<!-- Plugins JS start-->
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('assets/admin/js/script.js')}}"></script>
<!-- Plugin used-->
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/autocomplete.js')}}"></script>

@stack('scripts')
