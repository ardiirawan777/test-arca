<center>

    <button class="btn  btn-sm btn-warning show-form" data-size='{{ $size ?? "md" }}' data-url='{{ $url_edit }}' data-toggle="tooltip" title="Ubah Data">
        <i class="fa fa-edit"> </i>
    </button>
</center>