<center>
    <button class="btn btn-sm btn-primary show-form" data-size='{{ $size ?? "md" }}' data-url='{{ $url_show }}' data-toggle="tooltip" title="Lihat Detail">
        <i class="fa fa-eye"> </i>
    </button>
</center>