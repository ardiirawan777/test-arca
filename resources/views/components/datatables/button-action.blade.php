<center>
    <a href="{{ $url_show }}">
        <button class="btn  btn-sm btn-primary "   data-toggle="tooltip" title="Lihat Detail">
            <i class="fa fa-eye"> </i>
        </button>
    </a>
    <a href="{{ $url_edit }}">
        <button class="btn  btn-sm btn-warning "  data-toggle="tooltip" title="Ubah Data">
            <i class="fa fa-edit"> </i>
        </button>
    </a>

    <button class="btn  btn-sm btn-danger btn_delete" data-url='{{ $url_destroy }}' data-text="{{ $delete_text }}" data-toggle="tooltip" title="Hapus Data">
        <i class="fa fa-trash"> </i>
    </button>
</center>
