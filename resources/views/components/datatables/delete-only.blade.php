<center>
    <button class="btn  btn-sm btn-danger btn_delete" data-url='{{ $url_destroy }}' data-text="{{ $delete_text }}" data-toggle="tooltip" title="Hapus Data">
        <i class="fa fa-trash"> </i>
    </button>
</center>