<center>
    <button class="btn  btn-sm btn-primary show-form" data-size='{{ $size ?? "md" }}' data-url='{{ $url_show }}' data-toggle="tooltip" title="Lihat Detail">
        <i class="fa fa-eye"> </i>
    </button>
    <button class="btn  btn-sm btn-warning show-form" data-size='{{ $size ?? "md" }}' data-url='{{ $url_edit }}' data-toggle="tooltip" title="Ubah Data">
        <i class="fa fa-edit"> </i>
    </button>

    <button class="btn  btn-sm btn-danger btn_delete" data-url='{{ $url_destroy }}' data-text="{{ $delete_text }}" data-toggle="tooltip" title="Hapus Data">
        <i class="fa fa-trash"> </i>
    </button>
</center>