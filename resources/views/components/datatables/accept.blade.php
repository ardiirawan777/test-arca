<center>
    <button class="btn btn-sm btn-primary show-form" data-size = '{{ $size ?? "md" }}' data-url='{{ $url_edit }}' data-toggle="tooltip" title="Lihat Data">
        <i class="fa fa-check"> </i>
    </button>
</center>