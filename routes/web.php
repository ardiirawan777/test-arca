<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::post('ckeditor','CkeditorController@upload')->name('ckeditor.upload');

Route::get('/dashboard', function () {
    return view('pages.dashboard.index');
})->middleware(['auth', 'verified'])->name('dashboard');


Route::group(['middleware' => 'auth'],function (){
    Route::resource('employee','EmployeeController',['names' => 'employee']);
    Route::resource('bonus','BonusController',['names' => 'bonus']);

    Route::get('profile', 'ProfileController@index');
    Route::post('profile/update-profile', 'ProfileController@updateProfile');
    Route::post('profile/update-password', 'ProfileController@updatePassword');
});
Route::resource('category','CategoryController',['names' => 'category']);


require __DIR__.'/auth.php';
