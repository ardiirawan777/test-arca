<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if(!User::count()){
            User::create([
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('123123'),
                'role' => 'ADMIN'
            ]);

            User::create([
                'name' => 'user',
                'email' => 'user@gmail.com',
                'password' => bcrypt('123123'),
                'role' => 'USER'
            ]);
        }
    }
}
