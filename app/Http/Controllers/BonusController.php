<?php

namespace App\Http\Controllers;


use App\Http\Requests\BonusRequest;
use App\Models\Bonus;
use App\Models\Category;
use App\Models\Employee;
use App\Services\BonusService;
use CrudAjax;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Requests\CategoryRequest;

class BonusController extends CrudAjax
{
    protected $model  = Bonus::class;
    protected $url    = "bonus/";
    protected $folder = "pages.bonus.";
    protected $files  = false;
    private $facade;

    public function __construct(BonusService $facade)
    {
        $this->facade = $facade;
        parent::__construct($this);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return $this->datatable($request);
        }

        return view($this->folder . "index");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model->with('employees')->findOrFail($id);
        return renderToJson($this->folder . "show",compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return renderToJson($this->folder . "create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BonusRequest $request)
    {
        return $this->setFacade($this->facade)->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->model->findOrFail($id);

        return renderToJson($this->folder . "edit", compact("data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BonusRequest $request, $id)
    {
        return $this->setParams($id)
            ->setFacade($this->facade)
            ->change();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->setParams($id)
            ->delete();
    }

    /**
     * json data for datatable.
     *
     *
     * @return DataTables
     */
    public function datatable($request)
    {
        $datas = $this->model->query();

        return DataTables::of($datas)
            ->addIndexColumn()
            ->editColumn('created_at', function ($data) {
                return date_indo($data->created_at);
            })
            ->editColumn('ammount', function ($data) {
                return  number_format($data->ammount, 0, ',', '.');
            })
            ->addColumn('action', function ($data) {
                $params = [
                    'data'        => $data,
                    'size'        => 'lg',
                    'url_show'    => url($this->url . $data->id),
                    'url_edit'    => url($this->url . $data->id . '/edit'),
                    'url_destroy' => url($this->url . $data->id),
                    'detele_title'=> "data",
                    'delete_text' => view($this->folder . 'delete', compact('data'))->render()
                ];
                if(auth()->user()->role == 'ADMIN'){
                    return view('components.datatables.action-with-show', $params);
                }else{
                    return view('components.datatables.show-only', $params);
                }
            })
            ->make(true);
    }

}
