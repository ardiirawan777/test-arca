<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Http\Request;


class ProfileController extends Controller
{
    public function index()
    {
        return view("pages.profile.index");
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        auth()->user()->update([
            "name" => $request->name,
            "email" => $request->email
        ]);

        return back()->with(["success-profile" => true]);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        auth()->user()->update([
            "password" => bcrypt($request->password)
        ]);

        return back()->with(["success-password" => true]);
    }
}
