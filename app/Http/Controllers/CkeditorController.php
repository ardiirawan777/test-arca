<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CkeditorController extends Controller
{
    public function upload(Request $request)
    {
        try{
            if($request->has('upload')){
                $originalName = $request->file('upload')->getClientOriginalName();
                $fileName = pathinfo($originalName,PATHINFO_FILENAME);
                $extension = $request->file('upload')->getClientOriginalExtension();
                $fileName = $fileName.'-'.time().'.'.$extension;

                //

                Storage::putFileAs('images', $request->upload, $fileName);

                $CKEditorFuncNum = $request->input('CKEditorFuncNum');

                $url      = asset('storage/images/'.$fileName);
                $message  = 'Image Upload Successfully';
                $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum,'$url','$message')</script>";
                return response($response);
            }
        }catch(\Exception $e){
            dd($e);
        }

    }
}
