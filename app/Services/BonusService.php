<?php

namespace App\Services;

use App\Models\Bonus;
use App\Models\BonusEmployee;


class BonusService
{
    public function save($request)
    {
        $bonus = Bonus::create([
            'ammount' => $request->ammount,
        ]);

        for($i=0; $i < count($request->name); $i++){
            BonusEmployee::create([
                'bonus_id' => $bonus->id,
                'name' => $request->name[$i],
                'percen' => $request->percen[$i],
                'ammount' => $request->ammount * $request->percen[$i] / 100
            ]);
        }


    }

//    update data
    public function update($request,$id)
    {
        $data = Bonus::find($id);
        $data->update(['ammount' => $request->ammount]);

        $data->employees()->delete();

        for($i=0; $i < count($request->name); $i++){
            BonusEmployee::create([
                'bonus_id' => $data->id,
                'name' => $request->name[$i],
                'percen' => $request->percen[$i],
                'ammount' => $request->ammount * $request->percen[$i] / 100
            ]);
        }


    }
}
