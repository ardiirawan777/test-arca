<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class TotalPercenRule implements ValidationRule
{
    public function __construct($percen)
    {
        $this->percen = $percen;
    }
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $total_percen = array_sum($this->percen);

        if ($total_percen != 100) {
            $fail('Total Persen Harus 100%');
        }
    }
}
